package br.ucsal.bes20191.testequalidade.escola.business;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import br.ucsal.bes20191.testequalidade.escola.builders.AlunoBuilder;
import br.ucsal.bes20191.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20191.testequalidade.escola.persistence.AlunoDAO;
import br.ucsal.bes20191.testequalidade.escola.util.DateHelper;

public class AlunoBOUnitarioTest1 {

	private static AlunoDAO alunoDAOMock;
	private static DateHelper dateHelperMock;
 
	private static AlunoBO alunoBO;

	@BeforeClass
	public void setupClasse() {
		alunoDAOMock = Mockito.mock(AlunoDAO.class);
		dateHelperMock = Mockito.mock(DateHelper.class);
		alunoBO = new AlunoBO(alunoDAOMock, dateHelperMock);
	}
	
	@Before
	public void setup(){
		Mockito.reset(alunoDAOMock, dateHelperMock);
	}

	/**
	 * Verificar o calculo da idade. Caso de teste: aluno nascido em 2003 ter�
	 * 16 anos.
	 */
	@Test
	public void testarCalculoIdadeAluno1() {

		// Defini��o de dados de entrada
		Integer matricula = 123;
		Integer anoNascimento = 2003;
		Aluno aluno = AlunoBuilder.umAluno().comMatricula(matricula).nascidoEm(anoNascimento).build();
		Integer anoReferencia = 2019;
		Mockito.when(alunoDAOMock.encontrarPorMatricula(matricula)).thenReturn(aluno);
		Mockito.when(dateHelperMock.obterAnoAtual()).thenReturn(anoReferencia);

		// Defini��o do resultado esperado
		Integer idadeEsperada = 16;

		// Execu��o do m�todo que est� sendo testado e a obten��o do resultado
		// atual
		Integer idadeAtual = alunoBO.calcularIdade(matricula);

		// Compara��o do resultado esperado com o resultado atual
		Assert.assertEquals(idadeEsperada, idadeAtual);
	}

	/**
	 * Verificar se alunos ativos s�o atualizados.
	 */
	public void testarAtualizacaoAlunosAtivos() {
	}

}
