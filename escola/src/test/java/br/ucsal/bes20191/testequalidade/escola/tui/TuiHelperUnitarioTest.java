package br.ucsal.bes20191.testequalidade.escola.tui;

import java.io.PrintStream;
import java.util.Scanner;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class TuiHelperUnitarioTest {

	@Mock
	private Scanner scannerMock;

	@Mock
	private PrintStream printStreamMock;

	@InjectMocks
	private TuiUtil tuiUtil;

	/**
	 * Verificar a obten��o do nome completo. Caso de teste: primeiro nome
	 * "Claudio" e sobrenome "Neiva" resulta no nome "Claudio Neiva".
	 */
	@Test
	public void testarObterNomeCompleto() {
		String nome = "Claudio";
		String sobrenome = "Neiva";

		Mockito.when(scannerMock.nextLine()).thenReturn(nome).thenReturn(sobrenome);

		String nomeCompletoEsperado = "Claudio Neiva";
		String nomeCompletoAtual = tuiUtil.obterNomeCompleto();

		Assert.assertEquals(nomeCompletoEsperado, nomeCompletoAtual);
	}

	/**
	 * Verificar a obten��o exibi��o de mensagem. Caso de teste: mensagem "Tem
	 * que estudar." resulta em "Bom dia! Tem que estudar.".
	 */
	@Test
	public void testarExibirMensagem() {
		String mensagem = "Tem que estudar.";

		System.setOut(printStreamMock);

		tuiUtil.exibirMensagem(mensagem);

		// Verificar a chamada dos m�todos mocados, pois o exibirMensagem �
		// command
		Mockito.verify(printStreamMock).print("Bom dia! ");
		Mockito.verify(printStreamMock).println(mensagem);

	}

}
