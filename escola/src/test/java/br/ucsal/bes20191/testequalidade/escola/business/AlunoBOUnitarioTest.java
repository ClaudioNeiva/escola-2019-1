package br.ucsal.bes20191.testequalidade.escola.business;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import br.ucsal.bes20191.testequalidade.escola.builders.AlunoBuilder;
import br.ucsal.bes20191.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20191.testequalidade.escola.persistence.AlunoDAO;
import br.ucsal.bes20191.testequalidade.escola.util.DateHelper;

@RunWith(MockitoJUnitRunner.class)
public class AlunoBOUnitarioTest {

	@Mock
	private AlunoDAO alunoDAOMock;

	@Mock
	private DateHelper dateHelperMock;

	@InjectMocks
	private AlunoBO alunoBO;

	/**
	 * Verificar o calculo da idade. Caso de teste: aluno nascido em 2003 ter�
	 * 16 anos.
	 */
	@Test
	public void testarCalculoIdadeAluno1() {

		// Defini��o de dados de entrada
		Integer matricula = 123;
		Integer anoNascimento = 2003;
		Aluno aluno = AlunoBuilder.umAluno().comMatricula(matricula).nascidoEm(anoNascimento).build();
		Integer anoReferencia = 2019;
		Mockito.when(alunoDAOMock.encontrarPorMatricula(matricula)).thenReturn(aluno);
		Mockito.when(dateHelperMock.obterAnoAtual()).thenReturn(anoReferencia);

		// Defini��o do resultado esperado
		Integer idadeEsperada = 16;

		// Execu��o do m�todo que est� sendo testado e a obten��o do resultado
		// atual
		Integer idadeAtual = alunoBO.calcularIdade(matricula);

		// Compara��o do resultado esperado com o resultado atual
		Assert.assertEquals(idadeEsperada, idadeAtual);
	}

	/**
	 * Verificar se alunos ativos s�o atualizados.
	 */
	@Test
	public void testarAtualizacaoAlunosAtivos() {
		// Defini��o do dado de entrada
		Aluno alunoAtivo = AlunoBuilder.umAluno().ativo().build();

		// Execu��o do m�todo sob teste, mas n�o tenho como obter o resultado
		// atual! O m�todo atualizar � do tipo command, ou seja, n�o tem retorno
		// (void).
		alunoBO.atualizar(alunoAtivo);

		// Verifica��o da chamada do m�todo salvar() do alunoDAO.
		Mockito.verify(alunoDAOMock).salvar(alunoAtivo);
		// Mockito.verify(alunoDAOMock, Mockito.atLeast(3)).salvar(alunoAtivo);
		// Mockito.verify(alunoDAOMock, Mockito.times(5)).salvar(alunoAtivo);
		// Mockito.verify(alunoDAOMock,
		// Mockito.times(5)).salvar(Mockito.any(Aluno.class));
	}

}
